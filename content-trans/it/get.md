---
menu:
  main:
    name: Installa
    weight: 4
sassFiles:
- scss/get.scss
title: Distribuzioni che offrono Plasma Mobile
---
## Cellulare

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM è la distribuzione Manjaro, ma per dispositivi ARM. È basata su Arch Linux ARM, combinata con gli strumenti, i temi e l'infrastruttura di Manjaro per creare delle immagini di installazione per il tuo dispositivo ARM.

[Sito web](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Scarica:

* [Ultima versione stabile di (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Build degli sviluppatori (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installazione

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### openSUSE

![](/img/openSUSE.svg)

openSUSE, in precedenza SUSE Linux e SuSE Linux Professional, è una distribuzione Linux sponsorizzata da SUSE Linux GmbH e da altre aziende. Attualmente openSUSE fornisce build di Plasma Mobile basate su Tumbleweed.

##### Scarica

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Installazione

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) è un Alpine Linux ottimizzato per il tocco e preconfigurato che può essere installato su smartphone e su altri dispositivi mobili. Visualizza la [lista dei dispositivi](https://wiki.postmarketos.org/wiki/Devices) per vedere il progresso del supporto al tuo dispositivo.

Per i dispositivi che non hanno immagini precostituite, sarà necessario scriverla usando il programma «pmbootstrap». Segui [queste](https://wiki.postmarketos.org/wiki/Installation_guide) istruzioni. Controlla anche la pagina wiki del dispositivo per ulteriori informazioni su cosa funziona.

[Scopri di più](https://postmarketos.org)

##### Scarica:

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [L'ultima Edge di PinePhone](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Dispositivi della comunità](https://postmarketos.org/download/)

### rootfs di riferimento basato su Neon

![](/img/neon.svg)

Immagine basata su KDE Neon. Lo stesso KDE Neon è basato su Ubuntu 20.04 (focal). Questa immagine è basata sul ramo dev-unstable di KDE Neon e fornisce sempre l'ultima versione di KDE frameworks, di KWin e di Plasma Mobile compilate da git master.

Al momento non ci sono lavori in corso per mantenere le immagini.

##### Scarica:

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## Installazione

Scarica l'immagine, decomprimila e, usando «dd» oppure uno strumento grafico, scrivila su una scheda SD: PinePhone si avvierà automaticamente da lì. Per installare il contenuto, segui le istruzioni nel [wiki di Pine](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Dispositivi desktop

### Immagine ISO basata su Neon amd64

![](/img/neon.svg)

L'immagine ISO usa gli stessi pacchetti di Neon del rootfs di riferimento, solo compilati per amd64. Può essere provato sui tablet Intel non Android, sui PC e sulle macchine virtuali.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
