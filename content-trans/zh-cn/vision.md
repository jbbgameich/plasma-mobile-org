---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: 愿景
    parent: project
    weight: 1
title: Plasma Mobile 项目愿景
---
Plasma Mobile 致力于成为一款完整的自由开源手机软件系统。<br />它面向对隐私问题敏感的用户，让他们能重新掌控自己设备的信息和通讯。

Plasma Mobile 项目采取务实的态度，对第三方软件持开放态度，允许用户选择自己想要的应用和服务，并在不同的设备上提供始终如一的无缝体验。<br />Plasma Mobile 致力于实现开放标准，开发过程透明，且开放给任何人参与贡献力量。
