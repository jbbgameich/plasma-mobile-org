---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Visión
    parent: project
    weight: 1
title: Nuestra visión
---
Plasma Mobile aspira a convertirse en un completo y abierto sistema de software para dispositivos móviles.<br /> Está diseñado para devolver a los usuarios preocupados por la privacidad el control sobre su información y sus comunicaciones.

Plasma Mobile adopta un enfoque pragmático y es inclusivo para el software de terceras partes, permitiendo que el usuario escoja las aplicaciones y los servicios que quiera usar, a la vez que proporciona una experiencia sin fisuras en múltiples dispositivos.<br /> Plasma Mobile implementa estándares abiertos y se desarrolla en un proceso transparente que está abierto para que cualquiera pueda participar.
