---
menu:
  main:
    name: Instal·leu-lo
    weight: 4
sassFiles:
- scss/get.scss
title: Distribucions que ofereixen el Plasma Mobile
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM és la distribució Manjaro, però per a dispositius ARM. Està basada en Arch Linux ARM, combinada amb les eines, temes i infraestructura de Manjaro per a fer la instal·lació d'imatges per als dispositius ARM.

[Lloc web](https://manjaro.org) [Fòrum](https://forum.manjaro.org/c/arm/)

##### Baixada:

* [Darrera estable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Construccions de desenvolupador (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instal·lació

Per al PinePhone, es pot trobar informació genèrica al [wiki del Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### openSUSE

![](/img/openSUSE.svg)

openSUSE, antigament SUSE Linux i SuSE Linux Professional, és una distribució de Linux patrocinada per SUSE Linux GmbH i altres empreses. Actualment openSUSE proporciona construccions del Plasma Mobile basades en Tumbleweed.

##### Baixada

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instal·lació

Per al PinePhone, es pot trobar informació genèrica al [wiki del Pine64](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), és un Alpine Linux tàctil optimitzat i preconfigurat que es pot instal·lar en telèfons intel·ligents i altres dispositius mòbils. Reviseu la [llista de dispositius](https://wiki.postmarketos.org/wiki/Devices) per a veure el progrés per acceptar el vostre dispositiu.

Per a dispositius que no tenen imatges preconstruïdes, cal gravar la seva memòria flaix interna manualment amb la utilitat `pmbootstrap`. Seguiu les instruccions [aquí](https://wiki.postmarketos.org/wiki/Installation_guide). Assegureu-vos també de verificar la pàgina wiki del dispositiu per a trobar més informació quant a què funciona.

[Apreneu-ne més](https://postmarketos.org)

##### Baixada:

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Darrera «Edge» (PinePhone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Dispositius de la comunitat](https://postmarketos.org/download/)

### «rootfs» de referència basada en Neon

![](/img/neon.svg)

Imatge basada en KDE Neon. La pròpia KDE Neon està basada en Ubuntu 20.04 (focal). Aquesta imatge està basada en la branca «dev-unstable» de KDE Neon, i sempre distribueix la darrera versió dels Frameworks, KWin i Plasma Mobile de KDE compilats a partir de «git master».

No hi ha cap treball en curs per a mantenir les imatges ara mateix.

##### Baixada:

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## Instal·lació

Baixeu la imatge, descomprimiu-la i graveu-la a una targeta SD usant `dd` o una eina gràfica. El PinePhone arrencarà automàticament des de la targeta SD. Per a instal·lar a la memòria flaix interna, seguiu les instruccions del [wiki de Pine](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Dispositius d'escriptori

### Imatge ISO amd64 basada en Neon

![](/img/neon.svg)

Aquesta imatge ISO usa els mateixos paquets que la «rootfs» de referència basada en Neon, compilada per a amd64. Es pot provar en tauletes Intel no Android, PC i màquines virtuals.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
