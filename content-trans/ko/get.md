---
menu:
  main:
    name: 설치
    weight: 4
sassFiles:
- scss/get.scss
title: Plasma 모바일이 탑재된 배포판
---
## 휴대폰

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM은 Arm 장치용 Manjaro 배포판입니다. Arch Linux ARM을 기반으로 하며, Manjaro에서 제공하는 도구, 테마, 기반을 사용하여 Arm 장치에 설치할 수 있는 이미지를 만듭니다.

[웹사이트](https://manjaro.org) [포럼](https://forum.manjaro.org/c/arm/)

##### 다운로드:

* [최종 안정판(PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [개발자 빌드(Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### 설치

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### openSUSE

![](/img/openSUSE.svg)

openSUSE는 과거에 SUSE Linux, SuSE Linux Professional로도 알려져 있으며, SUSE Linux GmbH 및 기타 회사에서 지원하는 리눅스 배포판입니다. 현재 openSUSE에서는 Tumbleweed 기반 Plasma 모바일 빌드를 제공합니다.

##### 다운로드

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### 설치

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS(pmOS)는 스마트폰과 기타 모바일 장치에 설치할 수 있는 터치에 최적화된 Alpine 리눅스의 파생형입니다. 장치 지원 여부를 확인하려면 [장치 목록](https://wiki.postmarketos.org/wiki/Devices)을 참조하십시오.

미리 빌드된 이미지가 없는 장치를 사용하고 있다면 `pmbootstrap` 유틸리티를 사용하여 수동으로 플래시해야 합니다. [여기](https://wiki.postmarketos.org/wiki/Installation_guide)에 있는 안내 사항을 따르십시오. 진행하기 전에 장치 지원 상태를 장치별 위키 페이지에서 확인하십시오.

[더 알아보기](https://postmarketos.org)

##### 다운로드:

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [일일 빌드(Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [커뮤니티 장치](https://postmarketos.org/download/)

### Neon 기반 참조 rootfs

![](/img/neon.svg)

KDE Neon 기반 이미지입니다. KDE Neon은 우분투 20.04(Focal)를 기반으로 합니다. 이 이미지는 KDE Neon의 dev-unstable 브랜치를 기반으로 하며, 항상 KDE 프레임워크, KWin, Plasma 모바일의 git master 버전을 제공합니다.

현재 이 이미지는 관리되지 않고 있습니다.

##### 다운로드:

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## 설치

이미지를 다운로드, 압축 해제한 후 `dd` 명령이나 그래픽 도구로 SD 카드에 기록하십시오. PinePhone은 SD 카드에서 부팅됩니다. 내장 메모리에 설치하려면 [Pine 위키](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions)의 내용을 참조하십시오.

## 데스크톱 장치

### Neon 기반 amd64 ISO 이미지

![](/img/neon.svg)

이 ISO 이미지는 Neon 기반 rootfs와 같은 패키지를 사용하지만 amd64 환경으로 컴파일되었습니다. 안드로이드를 사용하지 않는 인텔/AMD 태블릿, PC, 가상 머신에서 실행 가능합니다.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
