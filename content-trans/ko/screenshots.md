---
jsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
layout: page
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/screenshots.scss
- scss/components/swiper.scss
screenshots:
- name: Plasma 모바일 홈 화면
  url: /screenshots/plasma.png
- name: KWeather, Plasma 모바일 날씨 프로그램
  url: /screenshots/weather.png
- name: Kalk, 계산기 프로그램
  url: /screenshots/pp_calculator.png
- name: Megapixels, 카메라 프로그램
  url: /screenshots/pp_camera.png
- name: Calindori, 달력 프로그램
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, 메모 작성 프로그램
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular 모바일, 만능 문서 뷰어
  url: /screenshots/pp_okular01.png
- name: Angelfish, 웹 브라우저
  url: /screenshots/pp_angelfish.png
- name: Nota, 텍스트 편집기
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, 그림 뷰어
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, 파일 관리자
  url: /screenshots/pp_folders.png
- name: VVave, 음악 재생기
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: 하드웨어
  url: /screenshots/20201110_092718.jpg
title: 스크린샷
---
다음 스크린샷은 PinePhone에서 실행 중인 Plasma 모바일에서 촬영했습니다.

{{< screenshots name="screenshots" >}}
