---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Dołącz do Przenośnej Plazmy
---
Jeśli chciałbyś współtworzyć świetne darmowe oprogramowanie na urządzenia przenośne, [dołącz do nas - zawsze znajdziemy dla ciebie jakieś zadanie](/contributing/)!

Grupy i kanały społeczności Przenośnej Plazmy:

### Kanały przeznaczone dla Przenośnej Plazmy:

* [![](/img/matrix.svg)Matrix (największy ruch)](https://matrix.to/#/#plasmamobile:matrix.org)

* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)

* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)

* [![](/img/mail.svg)Lista rozmów nt. Przenośnej Plazmy](https://mail.kde.org/mailman/listinfo/plasma-mobile)


### Kanały związane z Przenośną Plazmą:

* [![](/img/irc.png)#plasma na Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg)Halium na Telegramie](https://t.me/Halium)

* [![](/img/mail.svg)Lista rozmów nt. rozwoju Plazmy](https://mail.kde.org/mailman/listinfo/plasma-devel)
