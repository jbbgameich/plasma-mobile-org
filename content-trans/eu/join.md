---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Batu Plasma Mobilera
---
Gailu mugikorretarako software libre apartari lagundu nahi badiozu, [zatoz gurekin - beti dugu zeregin bat zuretzat](/contributing/)!

Plasma Mobile komunitateko taldeak eta kanalak:

### Plasma Mobilen berariazko kanalak:

* [![](/img/matrix.svg)Matrix (aktiboena)](https://matrix.to/#/#plasmamobile:matrix.org)

* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)

* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)

* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)


### Plasma Mobilekin zerikusia duten proiektuen kanalak:

* [![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg)Halium on Telegram](https://t.me/Halium)

* [![](/img/mail.svg)Plasma garapenerako posta zerrenda](https://mail.kde.org/mailman/listinfo/plasma-devel)
