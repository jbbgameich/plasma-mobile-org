---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Bisioa
    parent: project
    weight: 1
title: Gure Bisioa
---
Plasma Mobile-k, gailu mugikorretarako sistema oso eta software irekikoa izan nahi du.<br />Pribatutasunari erreparatzen dioten erabiltzaileei euren informazio eta komunikazioaren gaineko kontrola itzultzeko diseinatuta dago.

Plasma Mobile takes a pragmatic approach and is inclusive to 3rd party software, allowing the user to choose which applications and services to use, while providing a seamless experience across multiple devices.<br /> Plasma Mobile implements open standards and is developed in a transparent process that is open for anyone to participate in.
