---
layout: page
menu:
  main:
    name: MEG
    parent: project
    weight: 5
title: Galdera ohikoenak
---
Zerbatik ez du Plasma Mobilek Mer/Nemomobile erabiltzen?
----------------------------------------------

Plasma Mobile gailu mugikorretarako software plataforma bat da. Ez da sistema eragile bat, Qt5, KDE azpiegitura («Frameworks»), Plasma eta aplikazio multzoaren zati diren hainbat aplikaziok osatzen dute. Plasma Mobile ibil daiteke Mer banaketaren gainean, baina denbora eta baliabide faltagatik, probatarako eta garapenerako oinarri gisa, gure arreta, egun, KDE Neon-en PinePhone-an jartzen ari gara.

Android aplikazioak Plasma Mobilen ibil daitezke?
---------------------------------------

There are projects like [Anbox](https://anbox.io/) which is Android running inside Linux container, and use Linux kernel to execute applications to achieve near-native performance. This could be leveraged in the future to have Android apps running on top of a GNU/Linux system with the Plasma Mobile platform, but it's a complicated task, and as of *today*(September 6th, 2020) some distributions already support Anbox and you can run Plasma Mobile on top of those distributions.

Ibil dezaket Plasma Mobile nire gailu mugikorrean?
--------------------------------------------

Gaur egun, Plasma Mobile ondoko gailuetan dabil:

* **(Gomendatua) PinePhone:** KDE Neon-en oinarrituta, PinPhone-rako eraikitako irudi ofizialak eskaintzen ditugu. Plasma Mobile-ren [dokumentazioan](https://docs.plasma-mobile.org) informazio gehiago aurki dezakezu.

* **x86-oinarrikoa:** Plasma Mobile, Intel tableta batean, mahaigainekoan/magalekoan, edo alegiazko makina batean probatu nahi baduzu,  Neon-oinarriko x86_64 Plasma Mobile [irudia](https://www.plasma-mobile.org/get/) zuretzako da. Betiko instalatzeko moduari buruzko informazioa Plama Mobile-ren [dokumentazioan](https://docs.plasma-mobile.org) aurki daiteke.

* **postmarketOS devices:** postmarketOS is a touch-optimized, pre-configured Alpine Linux that can be installed on Android smartphones and other mobile devices. This project is in *very early stages of development* but it does currently offer support for a fairly wide range of devices, and it offers Plasma Mobile as an available interface. Please find your device from the [list of supported devices](https://wiki.postmarketos.org/wiki/Devices) and see what's working, then you can follow the [pmOS installation guide](https://wiki.postmarketos.org/wiki/Installation_guide) to install it on your device. Your mileage may vary, and it is **not** necessarily representative of the current state of Plasma Mobile.

* **Other:** Unfortunately support for Halium based devices had to be dropped recently (see [technical debt](/2020/12/14/plasma-mobile-technical-debt/)). This includes the previous reference device Nexus 5x.

Plasma Mobile instalatu dut, zein da saio-hasteko pasahitza?
---------------------------------------------------------

Zure PinePhone-an, Neon, instalatzeko gidoiaren bidez instalatu baduzu, pasahitza «1234» izan beharko litzateke, eta Konsole-n «passwd» exekutatuz alda dezakezu. Manjaro-n «123456» da. Hura aldatzean, gogoan izan giltzatze-pantailan zenbakiak baino ezin dituzula sartu.

x86 irudia erabiltzen ari bazara, ez da ezartzen pasahitz lehenetsirik, eta hura ezarri beharko duzu, Konsole-n «passwd» exekutatuz, ezertarako autentifikatzeko gai izateko.

Zein da proiektuaren egoera?
--------------------------------

Plasma Mobile, gaur egun, garapen sakonean dago eta ez dago eguneroko erabilera arrunterako pentsatua. Lagundu nahi baduzu, [elkartu](/findyourway) jokora.
