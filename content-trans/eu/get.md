---
menu:
  main:
    name: Instalatu
    weight: 4
sassFiles:
- scss/get.scss
title: Plasma Mobile eskaintzen duten banaketak
---
## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM, ARM gailuetarako Manjaro banaketa da. Arch Linux ARM-en oinarrituta dago, Manjaro tresnak, gaiak eta azpiegiturarekin konbinatuta, zure ARM gailuetarako irudiak instalatzeko.

[Webgunea](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

##### Zama-jaitsi:

* [Azken egonkorra (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Developer builds (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Instalazioa

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### openSUSE

![](/img/openSUSE.svg)

openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.

##### Zama-jaitsi

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

#### Instalazioa

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility. Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the device's wiki page for more information on what is working.

[Jakizu gehiago](https://postmarketos.org)

##### Zama-jaitsi:

* [PinePhone](https://images.postmarketos.org/pinephone/)
* [Latest Edge (Pinephone)](https://images.postmarketos.org/bpo/edge/pine64-pinephone/plasma-mobile/)
* [Community Devices](https://postmarketos.org/download/)

### Neon oinarriko erreferentzia «rootfs»

![](/img/neon.svg)

Image based on KDE Neon. KDE Neon itself is based upon Ubuntu 20.04 (focal). This image is based on the dev-unstable branch of KDE Neon, and always ships the latest versions of KDE frameworks, KWin and Plasma Mobile compiled from git master.

There is no ongoing work for maintaining the images at the moment.

##### Zama-jaitsi:

* [PinePhone](https://images.plasma-mobile.org/pinephone/)

## Instalazioa

Download the image, uncompress it and flash it to a SD-card using `dd` or a graphical tool. The PinePhone will automatically boot from a SD-Card. To install to the embedded flash, please follow the instructions in the [Pine wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

## Mahaigaineko gailuak

### Neon oinarriko amd64 ISO irudia

![](/img/neon.svg)

ISO irudi honek Neon-en oinarritutako erreferentziako «rootfs»ek erabiltzen dituen pakete berdinak erabiltzen ditu. Android-ez darabilten intel tabletetan, PCetan eta alegiazko makinetan erabil daiteke.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
