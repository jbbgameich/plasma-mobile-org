---
version: "21.05"
title: "Plasma Mobile Gear 21.05"
type: info
date: 2021-05-10
---

# Changelog

## Alligator

- Adopt to PlaMo Gear 21.05 versioning
- Add url for bugtracking
- Fix compilation
- Refactor type registration
- Add .cache to .gitignore
- Set style for windows
- Offer feed grouping
- Fix build
- Add more license information
- Add BSD-2-Clause license text
- [cli] Added functionality to add feed through command line
- Fix spacing and layout in feed list header
- Add Screenshots in AppStream
- Add missing includes

## Angelfish

- Adapt version to PlaMo Gear 21.05
- temporarily use QtFeedback fork to have a working flatpak
- metainfo: Correct license
- Update bug tracking url
- Update crates
- update crates
- Update crates
- Make filter list downloading use less memory
- Speed up compile times a bit
- Increase minimum required Qt version to 5.15
- useragent: Get rid of unnecessary string conversions and allocations
- Update crates
- Clean up AngelfishWebProfile
- adblock: Improve error handling of filterlist loading
- adblock: Remove unneccessary if let
- Update crates
- cmake: Add QtFeedback dependency
- adblock: Switch back to builtin psl
- Update crates
- Make sure not to load initial tabs in private mode
- Move initial tabs loading to C++
- ListWebView: Remove apparently no longer necessary hacks
- Revert &quot;Update crates&quot;
- Increase gesture threshold
- AngelfishWebProfile: Minor cleanup
- Update crates
- Add left/right navbar gestures
- Switch tab page into tab drawer
- Update crates
- Register as handler for http(s) URIs

## Calindori

- Adapt version to PlaMo Gear 21.05
- Play sound on notifications
- Show context drawer on long press
- Add option to create incidence on empty lists
- Remove not needed cmake functions
- Initiate the 1.5 release cycle
- Update screenshots
- Fix translations configuration

## Kalk

- Adapt version to PlaMo Gear 21.05
- Use aboutData from C++
- recognize other locales&#39; decimal separator
- Add autotest running in German
- Add metadata for appstream data
- Add some autotests
- output fraction as float
- Revert &quot;change the result of division between two integer to float&quot;
- change the result of division between two integer to float
- use property
- fix Keyboard input event bug
- Fix abnormal key height on the pinephone
- accept leading + and - as valid factor
- fix segfault on landscape mode switching to binary calculator
- Removed translation for numbers inside binary calculator output
- Convert the base values in Binary Calculator to numbers
- fix binary calculator expression panel hide while there is no input
- add Base display to BinaryCalculator result
- Added 3 different output formats for binary calculator Updated MathEngine to handle KNumber
- add hex output to KNumber
- Add page transitions
- Improve contrast for number buttons
- clang-format in gitignore
- fixed width for KNumber
- fix from history
- Make history page text more readable
- Use light font weight on display
- Use light font weight on keypad buttons
- update README
- adaptive NumberPad layout
- Revert &quot;calculate GridLayout&quot;
- Fix blurs from being too strong
- Use RectangularGlow instead of DropShadow for performance
- calculate GridLayout
- Add vibration to numpad
- Fix compile errors
- extend KNumber to include binary i/o
- add BinaryCalculator entry
- Adding Functionality to the Scientific Calculator Mode
- Removed additional page that was wrongly added to main.qml
- Added abs function to funtion pad
- Completed binary extension to Knumber Added keyboard shortcuts to most operators Binary operators completed Completed Integeral Binary Calculator without Unary operators
- Completed most of the grammar and syntax checks
- remove unused function
- Refactored to have a seperate mode varaible
- Added decimal point input
- Updated Input manager methods to accomodate the use of binary mode
- Add spdx
- Fix converter keypad height
- Fix layout of Unit Conversion page
- add swap units button
- switch data type to knumber
- fix compile error
- Made UI updates to Binary mode
- cmake modules from kcalc to find dependencies
- apply KDE review items
- copy knumber library from kcalc
- Added basic UI for binary calculator
- Shifted branches
- Binary Calculator UI
- Removed Local script from .gitignore
- Added to .gitignore for local buildscript ingoring Added Absolute Value functionality

## KClock

- Adapt version to PlaMo Gear 21.05
- Get rid of custom AboutData and use the single about data from main.cpp
- Use BasicListItem properties, as spacing will be added in Kirigami
- Update TimePage.qml
- Use BasicListItem for the delegate.
- Add i18n()
- Add bugtracker url
- Center plasmoid in bounding box
- Fix plasmoid text alignment
- Fix plasmoid configuration
- Improve time page placeholder message
- Use timesource for plasmoid time
- Add settings to plasmoid and rework appearance
- Fix arbitrary padding from lack of kclockd warning
- Fix stopwatch showing NaN on reset, and seconds alignment
- Add helpful add action to placeholders
- Use consistent names for navbar
- Change &quot;add a timer&quot; placeholder message to &quot;no timers configured&quot;
- Add warnings in kclock UI if kclockd is not running
- Remove version field from .desktop, as it is used for entry version, not app version
- Set minimum width and height for window
- silence -Wreorder
- add space between day and time in notifier hover
- add *.hpp to clang-format target
- order headers include, replace klocalizedstring.h by KLocalizedString
- run clang-format target
- clang-format git hook in cmake
- Add context drawer
- Add screenshots
- Make bottom bar have consistent colours
- Update metadata
- Improve performance of page switch animation
- Fix mobile sidebar toolbar for glitching text positioning
- Add page transition animations
- Don&#39;t switch page on sidebar if you are already on the page
- Fix empty deleted alarm notification
- Remove clock heading sidebar hack
- Streamline stopwatch page animation
- Add list item padding
- Fix stopwatch action
- Add ability to click on stopwatch time to toggle it
- Exchange reset and start/pause buttons on desktop stopwatch
- Fix i18n breaking ListModels
- Disable scrollbar on settings dialogs
- add missing i18n
- Add list animations
- Fix blurry icons
- Fix light font
- Add keyboard controls to timer and stopwatch
- Improve light clock font to use Noto Sans Light
- Fix
- Remove unnecessary utilmodel::applicationLoaded and time page timer not starting
- Remove highlight on stopwatch page buttons
- Fix timezoneselect height from being short on mobile
- Switch to overlaysheet/overlaydrawer for timezone select page
- Ensure consistent button sizes
- Switch to overlaysheet/overlaydrawer for new timer form
- Use reuseItems on ListViews
- Bump KF5 requirement to 5.79 and Qt requirement to 5.15
- Put analog clock in async loader
- Use consistent file names
- Add accessibility to buttons

## Kongress

- Adapt version to PlaMo Gear 21.05
- Add bugtracking url
- Simplify flatpak manifest
- Streamline favorites view
- Simplify conference selection
- Add placeholder icon to the Favorites page
- Offer favorites export
- Add generic name
- Remove cmake functions not needed

## KRecorder

- Adapt version to PlaMo Gear 21.05
- Correct metadata license
- Add more information
- Add a category
- Remove unnecessary version key from .desktop file
- Fix player page bars only using half the screen
- Move settings into drawer/sheet
- Insert new recordings from the beginning
- Fix recording bar widths
- Use center aligned recording component
- Update file headers
- Add mobile record drwaer
- Improve widescreen layout

## KTrip

- Adapt version to PlaMo Gear 21.05
- Require CMake 3.16
- Use DatePopup from kirigami-addons
- Remove unneeded ECM_KDE_MODULE_DIR
- Use version-less Qt targets
- Force breeze QStyle on Windows
- Default to org.kde.desktop QQC2 style
- Don&#39;t show two placeholders at the same time
- rerun clang-format
- Remove obsolete COPYING files
- Improve layouting in connection details
- Hide intermediate stops by default


## plasma-dialer

- Adapt version to PlaMo Gear 21.05
- Treat voicemail number like an implicit contact
- Start on dialer page
- Don&#39;t call voicemail while already in a call
- Clear inputted phone number after calling
- Call voicemail when 1 key held
- Prefill number on contact click instead of calling
- Specify sort order for call history
- Display IMEIs in an OverlaySheet
- Support calling *#06# to get IMEI
- fix typo: respond not resond
- Shorten hour part of duration
- Make it clear that there are no contacts with a phone number, not no contacts in general
- Use consistent bottom navbar colours
- Use light font on large dialer number text
- Fix blurry bottom toolbar icons
- Add latest calls in call history to the top of the list

## plasma-phonebook

- Adapt for PlaMo Gear 21.05 versioning
- Don't show birthday label if no birthday is set
- Handle abscence of year in birthday gracefully
- Show a contact's birthday
- Put personactions into a toolbar
- Fix birthday picker
- Add birthday field
- Add bugtracking url

## plasma-settings

- Adapt to PlaMo Gear 21.05 versioning
- Make various changes
- Correct type for variable
- Do not mess with SuspendType
- Powerdevil treats everything as seconds, besides mobile, adapt code
- Finish removing the Brightness commands
- Remove brightness configuration
- [modules/power] Remove unused KService dep
- Drop empty X-KDE-PluginInfo-Depends
- Fix comments
- Fix values stored on disk
- [modules/power] Remove service type definition from desktop2json call
- Fix minor typo
- Power Management for mobile
- Add notify flag when writing changes to 12/24 hour format
- modules: drop the appearance KCM
- Add keyboard theme setting

## qmlkonsole

- Adapt version to PlaMo Gear 21.05
- metainfo: Correct license
- Repeat arrow keys on hold
- Improve appearance of keyboard bar

## spacebar

- Adapt version to PlaMo Gear 21.05
- Update README
- Use random id when the real id is not known
- Remove leftover qDebugs
- Hack around wrong date parsing
- daemon: Add desktop file for autostart
- Add missing Q_EMIT
- Create MessageModel with parent
- Rename SOfonoMessageManager
- Use std::shared_ptr instead of QSharedPtr for consistency
- Remove telepathy config files
- Refactor database migrations
- Use QSharedPointer
- Use std::pair instead of tuple with two members
- Properly track progress of sending messages
- Add custom QOfonoMessageManager subclass with QFuture based API
- Port to libqofono
- Correct license headers to comply with the licensing policy
- Don&#39;t make notifications persistent
- Update bug tracking url
- Fix wrong message being displayed if search matches no results
- Move some files to new telephonySupport lib
- Implement deleting chats
- Add generic name
- Fixed a little bug that make Spacebar unable to be invoked as sms handler application by other programs
- Improve appearance of message cards and add spacing to chats list
- Fix window title

