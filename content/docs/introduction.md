---
title: Introduction
weight: 1
---

## Can I use it?

Yes! Plasma Mobile is currently preinstalled on the PinePhone using Manjaro ARM. There are also various other distributions for the PinePhone with Plasma Mobile available.

There is also postmarketOS, a touch-optimized Alpine Linux with support for many more devices, and while it’s in very early stages of development, it offers Plasma Mobile as an available interface for the devices it supports. You can see the list of supported devices here, but given the state of pmOS, your mileage may vary.

The interface is using KWin over Wayland and is now mostly stable, albeit a little rough around the edges in some areas. A subset of the normal KDE Plasma features are available, including widgets and activities, both of which are integrated into the Plasma Mobile UI. This makes it possible to use and develop for Plasma Mobile on your desktop/laptop.

## What can it do?

There are quite a few touch-optimized apps that are now being bundled with the Manjaro-based Plasma Mobile image, allowing a wide range of basic functions. These are mostly built with Kirigami, KDE’s interface framework allowing convergent UIs that work very well in a touch-only environment. You can find a list of these applications on [plasma-mobile.org homepage](https://plasma-mobile.org).

## Where can I find…

The code for various Plasma Mobile components can be found on [invent.kde.org](https://invent.kde.org/plasma-mobile). See [Get the source code](/get-source) for the individual repositories.

You can also ask your questions in the [Plasma Mobile community groups and channels](/join).

