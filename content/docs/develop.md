---
title: Develop For Plasma Mobile
weight: 3
---

We have multiple tutorials regarding how to develop an Plasma Mobile
application, how to debug the telephony features and more at
[develop.kde.org/docs/plasma-mobile](https://develop.kde.org/docs/plasma-mobile).


