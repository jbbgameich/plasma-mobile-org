---
author: Plasma Mobile team
created_at: 2020-12-14 12:20:00 UTC
date: "2020-12-14T00:00:00Z"
title: 'Plasma Mobile: Technical debt and moving forward'
---

In August 2015, we announced the Plasma Mobile project at Akademy 2015 in A Coruña, Spain. In ~5 years, the project has evolved quite a lot, from an initial prototype, to being a fully functional mobile software system.

The initial prototype was created by replacing components from the Ubuntu Touch userspace which ran on the Google Nexus 5 device. This prototype system used the minimal Android userspace confined in an LXC container to access various drivers, like graphics, modem, wifi, sensors among others.

In the following years, we revisited the initial prototype architecture multiple times. You can read about some of the details in the following blogposts by Bhushan Shah.

- [Plasma Mobile: New base system](https://blog.bshah.in/2016/05/02/plasma-mobile-new-base-system/)
- [Plasma Mobile running on Nexus 5X - bullhead](https://blog.bshah.in/2016/12/19/plasma-mobile-on-nexus-5x-bullhead/)

In 2017, several of communities working on bringing GNU/Linux to mobile devices came together and [introduced Project Halium](https://halium.org/announcements/2017/04/24/halium-is-in-the-air.html). This was ultimately what we used in the our reference Nexus 5X images based on KDE Neon.

In addition to this base system there were several components in userspace which we needed to use,

- [libhybris](https://github.com/libhybris/libhybris/) to allow using bionic based HW adaptions in glibc/muslc userspace system
- [hwcomposer backend](https://invent.kde.org/plasma/kwin/-/tree/master/plugins/platforms/hwcomposer) in KWin to use hardware acceleration
- [gst-droid](https://github.com/sailfishos/gst-droid) to access camera and hardware accelerated video playback
- [pulseaudio-modules-droid](https://github.com/mer-hybris/pulseaudio-modules-droid) for making pulseaudio work with android sound subsystem

While this worked and provided us with the base system that we can work against, and improve the shell, applications and other components, maintaining this was always uphill battle. Most of these components needed to depend on the binary blobs provided by the device vendors, required various device specific quirks or hacks that can not be upstreamed etc.

In last few years, several other companies have started working on open mobile hardware projects,

- [Pine64 PinePhone](https://forum.pine64.org/showthread.php?tid=7093)
- [Purism Librem 5](https://kde.org/announcements/kde-purism-librem5/)

In addition to that several communities including postmarketOS team [started porting several of off-shelf devices to run a mainline kernel](https://wiki.postmarketos.org/wiki/Mainlining).

These open devices provided us with better architecture and userspace much closer to our traditional "desktop" userspce. It also allowed us to use the same standards that we use on traditional Linux userspace like, DRM/GBM, V4L2, ALSA without depending on android components. This also allows us to have a userspace without binary blobs.

**After careful consideration, we have decided to drop support for Halium devices in Plasma Mobile.** We understand that some community members has been using the Halium based devices to play around with Plasma however, with limited resources and manpower we can not support or provide a guarantee of first class experience for Halium devices. In fact, this has been case for while now,

- Some of our team members no longer have access to reference LG Nexus 5X device anymore
- [After KDE Neon switched to using Ubuntu 20.04](https://blog.neon.kde.org/index.php/2020/08/10/kde-neon-rebased-on-20-04/) we no longer are updating the rootfs for halium devices
- After several important architecture changes in upstream KWin, the hwcomposer backend might be broken and we have no way of verifying it

Overall we are hopeful that [Plasma Mobile on open devices](https://blog.bshah.in/2018/03/26/plasma-mobile-and-open-devices/) will flourish and will bring us much closer to our initial vision of Plasma Mobile.
