---
author: Plasma Mobile team
created_at: 2020-05-26 12:00:00 UTC+2
date: "2020-05-26T00:00:00Z"
title: 'Plasma Mobile update: April-May 2020'
---

It's been a while since the last status update on Plasma Mobile, so let's take a look at what happened since then.

To assist new people in contributing, we organized a virtual mini Plasma Mobile sprint in April. During the three days, we discussed many things, including our current tasks,
the websites and documentation, our apps and many other topics. Most of our important tasks have been asigned to people, many of them have been implemented already.

On Saturday, there was a training day, with four training sessions on the technology behind Plasma Mobile:
 - [Introduction to Kirigami](https://peertube.mastodon.host/videos/watch/ea598f81-fbbb-4010-96e0-952f897d473e)
 
 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/ea598f81-fbbb-4010-96e0-952f897d473e" frameborder="0" allowfullscreen></iframe>

 - [Porting KDE Applications to Android](https://peertube.mastodon.host/videos/watch/d257ec00-c029-4448-bce4-e6c750d87738)
 
 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/d257ec00-c029-4448-bce4-e6c750d87738" frameborder="0" allowfullscreen></iframe>

 - [Developing Telephony Functionality using phonesim](https://peertube.mastodon.host/videos/watch/84f24bea-af4c-4ae3-8ef3-7ff587ea17cf)
 
 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/84f24bea-af4c-4ae3-8ef3-7ff587ea17cf" frameborder="0" allowfullscreen></iframe>

 - [How to run the Plasma Mobile Shell on your desktop](https://peertube.mastodon.host/videos/watch/78fb236c-931e-4051-99db-1d203203d673)
 
 <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/78fb236c-931e-4051-99db-1d203203d673" frameborder="0" allowfullscreen></iframe>

While the Sprint was not active, we were busy working on Plasma Mobile itself.

Nico mostly worked on fixing bugs. First, [files now open correctly in Okular](https://invent.kde.org/graphics/okular/-/merge_requests/150)
Also, [the busy indicator made invisible when it's not running](https://invent.kde.org/frameworks/plasma-framework/-/commit/bc3ccd1782cb59f00cad089dfb5abb1594c6c5da)
In ruqola, he [fixed](https://invent.kde.org/network/ruqola/-/commit/75c0309198247bd534694ba22f70484af7e15b01) a few [bugs](https://invent.kde.org/network/ruqola/-/commit/802bf35bb5fa53a406ff0decc6667d1952dd22da) regarding the layout on small screens.

KTrip now has all providers disabled by default, to make it easier to have a good selection of enabled providers. The list of available connections and the connection details have been redesigned.

![Redesigned connections view](/img/ktrip-connections-redesign.png){: .blog-post-image-small}
![Redesigned connection details](/img/ktrip-connections-redesign2.png){: .blog-post-image-small}

It now also allows to open a station's position on a map.

Tobi has been busy working on the wireless connectivity. Some refactoring and bugfixes allow [Wi-Fi connections to be editable now](https://phabricator.kde.org/D28799)
and to create custom connections, enabling users to connect to hidden Wi-Fi networks. Also, there is now a [settings module for creating Wi-Fi hotspots](https://phabricator.kde.org/D29019)

Apart from the work on the wireless settings, he put a lot of work into [Alligator](https://invent.kde.org/kde/alligator), the RSS feed reader for Plasma Mobile. Even though it's been in development since the previous Plasma Mobile Sprint in February, it's never really been anounced here. By now, most basic functionality is working, and it's not even slow anymore!

In the last few days especially, a lot of work was put into things like [porting it to Android](https://invent.kde.org/kde/alligator/-/commit/26c57d45d7ca99445e9265d31262db78af207f62), [getting an (amazing!) icon](https://phabricator.kde.org/D29070) and getting a proper KDE repository for it. With some more polish, it will be ready for a first release soon.

Dimitris has put a lot of work into calindori. [A day view has been created](https://invent.kde.org/kde/calindori/-/commit/e30697f51ef701f476740ba071e3d46eff93cb59), offering the users the opportunity to review the incidences (events or tasks) of each day. In specific, the incidences are presented in an hour list, according to their start and in case of events end time. The users may navigate between the days as well as return to the current day, using the action toolbar buttons.
![Day View](/img/calindori_dayview.png){: .blog-post-image-small}

[A Week view is now available](https://invent.kde.org/kde/calindori/-/commit/c1627a69b073c4804a7042ee554005e2f17b6b8f); users can manage tasks and events via a per-week dashboard. On that dashboard, a list view displays the days of each week. The tasks and events of each day can be seen. Upon clicking to a task/event, a new page is opened. On that page, the task/event can be edited or deleted. Users can navigate between weeks using the action toolbar icons.
![Week View](/img/calindori_weekview.png){: .blog-post-image-small}

Bart has made the [wifi](https://invent.kde.org/kde/plasma-phone-components/-/merge_requests/57) and [wwan](https://invent.kde.org/kde/plasma-phone-components/-/merge_requests/60) buttons in the top drawer toggles instead of opening the settings pages.

Marco [implemented a webapp-container in Angelfish](https://invent.kde.org/kde/plasma-angelfish/-/commit/6092d8839b83b33434bb49a9f74ab59ff43e61b0), which can be used to ship websites as webapps in Plasma Mobile images.

Jonah later [added functionality to Angelfish to allow adding webapps to the homescreen](https://invent.kde.org/kde/plasma-angelfish/-/commit/ebacbd7b20bc8884756422bb4ef5ff8f7ba2e5cb). As webapps don't have any browser user interface apart from the html content, he added [a retry button ](https://invent.kde.org/kde/plasma-angelfish/-/commit/787c22894ea8a6d4d15ee67051bc1779b577dc41) to the error handler, to allow reloading in case of errors. Jonah also adapted Angelfish's use of overlay sheets to the new design from Kirigami. The same was done with the time settings module of the settings app.

Recently he managed to [workaround a bug](
https://invent.kde.org/plasma-mobile/plasma-phone-settings/-/commit/897f36d56b531ead0c7039e14e4e03e5717934f6) causing scrolling in all apps using QtWebEngine, notably Angelfish, to jump around. This was caused by frames being shown in the wrong order. Fixing this was only possible thanks to the debugging of this issue Rinigus Saar did for SailfishOS.
Over the last month, he started to rewrite the SMS app SpaceBar, because it became very hard to debug, and the KDE Telepathy API it was using wasn't well suited for SMS. The new app is directly based on TelepathyQt, just like the dialer. New features include mapping of contacts to phone numbers of incoming conversations, starting chats without having to add a contact for the number, visual and functional improvements. Anthony Fieroni [fixed incoming conversations](https://invent.kde.org/jbbgameich/spacebear/-/commit/6446a7e72182a61521a6a699ac05a9e562daba18) in the new app.

While the app is already working better than SpaceBar, we are still working on fixing the last bugs and integrating it with the dialer internally.
Jonah also [updated the design of the application headers](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/1), which are used across all Kirigami apps according to the suggestions of the KDE Visual Design Group.

Rinigus [redesigned the highlight of the current tab](https://invent.kde.org/kde/plasma-angelfish/-/commit/314a3fa0b609acdfac9bb80bb17fa7aed73c358f) in Angelfish, and implemented an [overlay which shows the history of the current tab](https://invent.kde.org/kde/plasma-angelfish/-/commit/6b78bb4f4514fa4d7717b94e1846968a8b7c3e8c).

Nico worked on improving the dialer UI, which now makes use of the new SwipeNavigator component from Kirigami.
![Redesigned dialer](/img/dialer-swipenavigator.png){: .blog-post-image-small}

### Want to help?

This post shows what a difference new contributors can make. Do you want to be part of our team as well? Just in case we prepared [a few ideas for tasks new contributors](https://invent.kde.org/dashboard/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Mobile&label_name[]=Junior%20Job) can work on. Most coding tasks require some QML knowledge. [KDAB's](https://www.kdab.com/) [QML introduction video series](https://youtu.be/JxyTkXLbcV4) is a great resource for that!
