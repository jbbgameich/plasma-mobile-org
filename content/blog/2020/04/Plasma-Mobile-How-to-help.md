---
author: Plasma Mobile team
created_at: 2020-04-02 09:50:00 UTC+5.30
date: "2020-04-02T00:00:00Z"
title: 'Plasma Mobile: How to help us!'
---

![](/img/road-asphalt-space-sky-56832_lb.jpg)

# Status of Plasma Mobile

We often get asked: "how long until the 1.0 release?". Or: "how far away is Plasma Mobile 1.0?". The usual answer to both these question is "It'll be ready when it is ready". But, really, how do we know that it is ready?

Recently some of us prepared a check list of items which we consider necessary before we can declare Plasma Mobile "ready" or at rc1 status.

## Basic Applications

We currently have applications that cover many basic needs, such as:

- Playing music: [vvave](https://invent.kde.org/kde/vvave)
- Viewing pictures: [Koko](https://phabricator.kde.org/source/koko/) or [Pix](https://invent.kde.org/kde/maui-pix)
- Taking notes: [buho](https://invent.kde.org/kde/buho)
- Managing calendars: [calindori](https://invent.kde.org/kde/calindori)
- Managing files: [Index](https://invent.kde.org/kde/index-fm/)
- Viewing documents: [Okular](https://invent.kde.org/kde/okular/-/tree/master/mobile)
- Getting new software: [Discover](https://phabricator.kde.org/source/discover/)
- Sending SMS: [Spacebar](https://invent.kde.org/kde/spacebar)
- Managing contacts: [plasma-phonebook](https://invent.kde.org/kde/plasma-phonebook/)
- Making phone calls: [plasma-dialer](https://invent.kde.org/kde/plasma-dialer/)
- Browsing the Internet: [plasma-angelfish](https://invent.kde.org/kde/plasma-angelfish)

There are some more applications which are currently in individual developer's personal namespace and not yet moved to the official repositories:

- A [videoplayer](https://invent.kde.org/jbbgameich/videoplayer/)
- A [clock application](https://invent.kde.org/nickre/kirigamiclock)
- A calculator application: [kalk](https://invent.kde.org/cahfofpai/kalk/)
- An audio recorder: [voicememo](https://invent.kde.org/jbbgameich/voicememo)

Most of these applications have some bugs or missing features, for example:

- [Several issues with the spacebar](https://phabricator.kde.org/T12899)
- Calindori should use the [timer_fd kernel interface](https://phabricator.kde.org/T12900) so it can send alarms even when the phone is asleep
- It should be possible to [answer phone calls when the screen is turned off/locked](https://phabricator.kde.org/T12885)

We also need to install some common chat apps, like [Telegram](https://desktop.telegram.org/) and [Spectral](https://gitlab.com/spectral-im/spectral).

## KWin Wayland compositor

There are some tasks in KWin Wayland that we hope to get resolved before the 1.0 version of Plasma Mobile,

- Support for [partial updates in the compositor](https://phabricator.kde.org/D27788) will improve performance and battery life
- [Window thumbnails for task switcher](https://phabricator.kde.org/T9233)
- Support for the [input-method-unstable-v1 protocol support](https://phabricator.kde.org/D27338) is needed to enable keyboard input with some third-party apps

Also, performance profiling/improvements in KWin are very welcome.

## General Plasma shell tasks

- We need [support for notifications on the lockscreen](https://phabricator.kde.org/T12884). There is ongoing work in this direction (with patches for [plasma-workspace](https://phabricator.kde.org/D28454), [kscreenlocker](https://phabricator.kde.org/D28455) and [plasma-phone-components](https://invent.kde.org/kde/plasma-phone-components/-/merge_requests/68)), however we are still [discussing the best implementation](https://mail.kde.org/pipermail/plasma-devel/2020-April/114374.html).

## Configuration Modules

Currently we have a basic set of system configurations:

- Date and time configuration
    - NTP based automatic date/time selection
    - Allow to set 24 hour clock
    - Manual datetime and timezone selection
- Basic Wifi settings
- Language settings
- Support for Nextcloud and Google accounts
- Basic system information

However, we are still missing some configuration needs:

- [Automatic date and time from mobile network](https://phabricator.kde.org/T12889)
- [Configuration of the audio levels and notifications](https://phabricator.kde.org/T6940)
- [Showing basic information about IMEI number and MAC address](https://phabricator.kde.org/T12890)
- [Showing SIM and network information](https://phabricator.kde.org/T12891)
- [Support for wifi network with security other than WPA2-PSK](https://phabricator.kde.org/T12892)
- [Ability to connect to hidden WiFi networks](https://phabricator.kde.org/T12893)
- [Mobile data configuration](https://phabricator.kde.org/T12894)
- [Wireless hotspot](https://phabricator.kde.org/T12895)
- [Improved user experience of language settings](https://phabricator.kde.org/T12896)
- [Settings module for bluetooth configuration](https://phabricator.kde.org/T12897)
- [Keyboard layout configuration](https://phabricator.kde.org/T12904)
- [Screen lock and PIN configuration](https://phabricator.kde.org/T12898)
- [Energy saving configuration](https://phabricator.kde.org/T12888)

## We need your help!

We need your help to reach the 1.0 release goal. Some of these tasks are really easy to implement and a great way to get involved. If you are interested in helping us out, you can contact us at

- [#plasmamobile:kde.org](https://matrix.to/#/#plasmamobile:kde.org) on Matrix
- [@plasmamobile](https://t.me/plasmamobile) on Telegram
- #kde-plasmamobile on freenode

We will help you find a suitable issue you can help us with. You can also find the [full list of issues on phabricator](https://phabricator.kde.org/tag/plasma:_mobile/).

Also check out these resources to help you get started with [Plasma Mobile development](https://docs.plasma-mobile.org).

We are planning a mini online sprint that will happen next week so we can finish some of the tasks mentioned above and help with the onboarding of new developers. If you are interested, take a look at the [phabricator task](https://phabricator.kde.org/T12906).