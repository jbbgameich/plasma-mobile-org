---
author: Plasma Mobile team
created_at: 2020-03-09 15:30:00 UTC+5.30
date: "2020-03-09T00:00:00Z"
title: Plasma Mobile Sprint 2020 in Berlin
---

Like [last year](https://www.plasma-mobile.org/2019/03/11/Plasma-Mobile-Sprint-in-Berlin.html) the Plasma Mobile team met in [KDAB's](https://www.kdab.com/) office in Berlin from 3rd to 9th February for the second Plasma Mobile sprint.

Our work spans many areas:

## Shell and Design

Marco Martin has been re-working the shell user interface. Check out his [blog](https://notmart.org/blog/2020/02/fosdem-and-plasma-mobile-sprint/) for more details. 

<video controls="" src="/video/plamo-shell-pinephone.mp4" height="400">Plasma Mobile on PinePhone</video>

Thanks to Mathis Brüchert many Plasma Mobile apps have [new icons](https://social.anoxinon.de/@m4TZ/103625049438471001). Mathis also worked on updating the Plasma Mobile website. The ['Find your way' page's appearance](https://www.plasma-mobile.org/findyourway) is now consistent with the rest of the website.

![New icons](/img/icons-breeze-plamo.png){: .blog-post-image-small}

Jonah worked on a mobile-friendly open/save file dialog. Linus added quick access to known places like in Dolphin to this new file dialog.

![File dialog open](/img/filedialog_open.png){: .blog-post-image-small}
![File Save dialog](/img/filedialog_save.png){: .blog-post-image-small}
![Sidebar](/img/filedialog_sidebar.png){: .blog-post-image-small}

Linus also worked on [adding a screenshot action](https://invent.kde.org/kde/plasma-phone-components/-/merge_requests/46) to the top drawer.

Marco Martin [fixed an issue in kdeclarative](https://commits.kde.org/kdeclarative/1270e357a96f1e78281c0335b1bd05e5c9c3e3c6) that was causing the wifi settings to show an empty message box.

## KWin/Wayland

Aleix Pol implemented the [zwp_input_method_context_v1](https://cgit.freedesktop.org/wayland/wayland-protocols/tree/unstable/input-method/input-method-unstable-v1.xml) protocol in KWin/Wayland, allowing us to make use of the [maliit](https://maliit.github.io/) keyboard, [ibus](https://github.com/ibus/ibus/commit/5136dbc59fd3048445826c534fd6b5f3f8ca6b1e) and other input methods in Plasma. Aleix also implemented [EGL_KHR_partial_update](https://www.khronos.org/registry/EGL/extensions/KHR/EGL_KHR_partial_update.txt) which will yield better performance on ARM GPUs that support tiled rendering. Tobias Fella [fixed the virtual keyboard staying open when the screen is locked](https://phabricator.kde.org/D27172).

![Maliit in Plasma Wayland session](/img/plasma-mobile-kwin-maliit.jpg){: .blog-post-image-small}

## Applications

Jonah implemented audio visualization in [Voicememo, Plasma Mobile's audio recorder application](https://invent.kde.org/jbbgameich/voicememo/). He also reviewed a patch by Rinigus Saar that rewrites the tab switcher of Angelfish, Plasma Mobile's browser application. Tobias started work on a [RSS feed reader](https://invent.kde.org/tfella/alligator/). Our camera application now has a clear indication when the properties are checkable. Nicolas worked on cleaning up the dialer's codebase.

![Alligator, RSS feed reader](/img/alligator-app.jpg){: .blog-post-image-small}

cahfofpai maintains a [list of mobile GNU/Linux applications](https://mglapps.frama.io/). During the sprint this list was improved. The list now includes new applications like KTrip, Kamoso, voicememo, Ruqola, Kongress, Keysmith and Colour Palette, and some duplicate applications were removed. Workflow for editing the list [was improved](https://framagit.org/mglapps/mglapps.frama.io/-/commit/e79c3b9157819ca4bb497aa723f82549dc2ee6ac)

cahfofpai and Jonah tested various mobile-friendly GNOME applications on KDE Neon image and found out [which things need improvement](https://phabricator.kde.org/T12781). cahfofpai also figured out a bug in Kaidan's flatpak recipe which prevented Kaidan from detecting the camera. Bhushan Shah along with cahfofpai reviewed the list of currently pre-installed applications in Plasma Mobile.

Jonah and cahfofpai tested Flatpak support on PinePhone KDE Neon image and [found out that there are no arm64 flatpak builds created by binary factory](https://invent.kde.org/sysadmin/binary-factory-tooling/-/merge_requests/62).

## Kirigami

Camilo and Marco worked on expanding some Kirigami features inspired by Maui apps. This includes pull back headers and footers, which is a common pattern on Android that help focussing the app's main content on small screens. They also fixed some issues in Kirigami's ActionToolBar which is used in Maui's SelectionBar.

Maui apps now allow lasso selection when a mouse or keyboard is present, improving the support for desktop systems while keeping them touch-friendly.

## Android

There's an ongoing effort to make our mobile apps available on Android to grow the target audience and allow people to work on/test our apps without a Linux phone. Thanks to Nicolas you now can get [Kaidan](https://invent.kde.org/kde/kaidan/), [qrca](https://invent.kde.org/kde/qrca/), [Keysmith](https://invent.kde.org/kde/keysmith), and [Kongress](https://invent.kde.org/kde/kongress) for Android from [KDE's binary factory](https://binary-factory.kde.org/view/Android/) and [F-Droid repository](https://community.kde.org/Android/FDroid). He also [fixed an issue](https://phabricator.kde.org/D27239) with KNotifications on Android that affected Kaidan. Volker Krause, Nicolas and Aleix discussed upgrading our Android builds to Qt 5.14, which promises various improvements. cahfofpai investigated why KDE Itinerary fails to import files with special characters in the name

## Collaboration

This year we were joined by two [UBports](https://ubports.com/) developers, [Marius Gripsgard](https://github.com/mariogrip) and [Dalton Durst](https://github.com/UniversalSuperBox). The main goal was to foster collaboration between the KDE and UBports communities. We discussed sharing software components for common needs. This includes solutions for [content sharing](https://github.com/ubports/content-hub), [telephony](https://github.com/ubports/telephony-service/), [permission management](https://github.com/ubports/trust-store), [push notifications](https://github.com/ubports/ubuntu-push) and [configuration management](https://cgit.kde.org/kconfig.git/).

We also discussed how to get KDE applications work in the Ubuntu Touch and vice versa. Jonah worked on [Ubuntu Touch flatpak runtime](https://gitlab.com/JBBgameich/ubuntu-touch-flatpak-runtime/) to enable running Ubuntu Touch application in various mobile distributions. Nicolas nagged Dalton and Marius into upgrading the Qt version shipped with Ubuntu Touch, which will allow KDE apps to work there. We also got a proof-of-concept click package for KTrip.

On Saturday, we joined the UBports Q&A, bi-weekly show hosted by UBports team, where we discussed work done at sprint,

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/cbfFQnFXvak" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Community

On 7th evening we went to dinner with local KDE community in Berlin.

![dinner](/img/dinner-berlin.jpg){: .blog-post-image-small}

## Conclusion

We would like to thank [KDE e.V.](https://ev.kde.org/) for supporting the event, [KDAB](https://kdab.com) for hosting us this week. We also like to thank Marius and Dalton from [UBports](https://ubports.com) for joining us. If you are interested in supporting our work, you can help us with [various tasks](https://phabricator.kde.org/tag/plasma:_mobile) or you can [donate to KDE](https://kde.org/donations).
